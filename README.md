# Cobol Hourly Payroll Calculator
How to Run COBOL Programs on Linux:

Install a COBOL Compiler: On Linux, the GNU COBOL compiler is a popular choice. You can install it using your distribution's package manager. For example, on Debian-based systems (like Ubuntu), you would use:

    sudo apt update
    sudo apt install gnucobol

Compile the COBOL Program: Use the GNU COBOL compiler (cobc) to compile your program. Open a terminal, navigate to the directory containing your .cob file, and run:

    cobc -x -free -o payroll "Payroll Calculator.cob"

This command compiles the Payroll_Calculator.cob program into an executable named payroll. The `-x` flag tells cobc to generate an executable, `-free` is used for free-format source code, and `-o Payroll_Calculator` specifies the output filename.

Run the Program: Once compiled, you can run the program directly from the terminal:

    ./payroll

Follow the Prompts: The program will ask you to enter the employee's name, hours worked, and hourly rate. After inputting this information, it will display the calculated gross pay, tax deducted, and net pay.

This example provides a basic structure for a payroll calculator in COBOL, demonstrating how to handle input, perform arithmetic operations, and output results. Keep in mind that real-world payroll calculations can be significantly more complex, involving various tax rates, benefits deductions, overtime calculations, and more.